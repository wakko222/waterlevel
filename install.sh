#!/bin/sh
sudo apt -y install git ntfs-3g exfat-fuse usbmount lsof samba python3-pip
sudo pip3 install pyserial
sudo rm /etc/localtime
sudo ln -s /usr/share/zoneinfo/America/Denver /etc/localtime
sudo cp $HOME/waterlevel/usbmount.conf /etc/usbmount/
sudo cp $HOME/waterlevel/50-usbmount.rules /etc/udev/rules.d/
sudo udevadm control --reload
sudo cp $HOME/waterlevel/smb.conf /etc/samba/smb.conf
sudo systemctl enable smbd nmbd
sudo systemctl restart smbd nmbd
curl -fsSL https://raw.githubusercontent.com/arduino/arduino-cli/master/install.sh | sh
sudo cp $HOME/waterlevel/bin/arduino-cli /usr/local/bin/
arduino-cli config init
rm -Rf $HOME/waterlevel/bin
cd $HOME
mkdir -p $HOME/waterlevel/arduino/waterlevel
arduino-cli core update-index
arduino-cli core install arduino:avr
arduino-cli lib install onewire dallastemperature
arduino-cli compile --fqbn arduino:avr:uno waterlevel
arduino-cli upload -p /dev/ttyACM0 --fqbn arduino:avr:uno waterlevel
mkdir -p $HOME/.config/systemd/user
cp $HOME/waterlevel/waterlevel.service $HOME/.config/systemd/user/
systemctl enable --user waterlevel.service
systemctl start --user waterlevel.service
#python3 $HOME/waterlevel/gettemptocsv.py &
