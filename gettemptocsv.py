#!/usr/bin/python3
import serial
import csv
import os.path
from datetime import date
from datetime import datetime
deg = u"\N{DEGREE SIGN}"
if __name__ == '__main__':
    ser = serial.Serial('/dev/ttyACM0', 9600, timeout=1)
    ser.flush()
    while True:
        if ser.in_waiting > 0:
            line = ser.readline().decode('utf-8').rstrip()
            line2 = (line +deg +'F')
            today = date.today()
            today2 = today.strftime('%B %d, %Y')
            time = datetime.now()
            time2 = time.strftime('%H:%M:%S')
            header = ['DATE:', 'TIME:', 'WATERLEVEL:']
            data = [today2, time2, line2]
            usbdrive = "/home/pi/local/"
            waterlevel = 'waterlevel'
            currentfile = os.path.isfile(usbdrive + "waterlevel" + '.csv') 
            with open(usbdrive + waterlevel + '.csv', 'a', encoding='UTF8') as tempoutput:
                writer = csv.writer(tempoutput)
                if not currentfile:
                    writer.writerow(header)
                writer.writerow(data) 
